#!/usr/bin/env bash

set -e

function section {
  echo "###########################################################"
  echo "###########################################################"
  echo "$1"
  echo "###########################################################"
  echo "###########################################################"
}

section "Installing Squid"

VERSION=3.5.28

# https://wiki.squid-cache.org/KnowledgeBase/CentOS#Compiling
yum install -y perl gcc autoconf automake make sudo wget
yum install -y libxml2-devel libcap-devel
yum install -y libtool-ltdl-devel

# curl -k https://artefactrepository.service.ops.iptho.co.uk/repository/EBSA/squid-${VERSION}.tar.gz > /opt/squid-${VERSION}.tar.gz
curl -k https://fossies.org/linux/www/squid-${VERSION}.tar.gz > /opt/squid-${VERSION}.tar.gz

umask 22

cd /opt
df -h /opt
df -h
ls -l /opt/squid-${VERSION}.tar.gz
tar -zxf /opt/squid-${VERSION}.tar.gz
echo "Post unpack"
cd /opt/squid-${VERSION}
echo "Executing configure"
./configure \
  --build=x86_64-redhat-linux-gnu \
	--host=x86_64-redhat-linux-gnu \
	--target=x86_64-redhat-linux-gnu \
	--program-prefix= \
	--prefix=/usr \
	--exec-prefix=/usr \
	--bindir=/usr/bin \
	--sbindir=/usr/sbin \
	--sysconfdir=/etc \
	--datadir=/usr/share \
	--includedir=/usr/include \
	--libdir=/usr/lib64 \
	--libexecdir=/usr/libexec \
	--sharedstatedir=/var/lib \
	--verbose \
	--exec_prefix=/usr \
	--libexecdir=/usr/lib64/squid \
	--localstatedir=/var \
	--datadir=/usr/share/squid \
	--sysconfdir=/etc/squid \
	--with-logdir='$(localstatedir)/log/squid' \
	--with-pidfile='$(localstatedir)/run/squid.pid' \
	--disable-dependency-tracking \
	--enable-follow-x-forwarded-for \
	--enable-auth \
	--enable-cache-digests \
	--enable-cachemgr-hostname=localhost \
	--enable-delay-pools \
	--enable-epoll \
	--enable-icap-client \
	--enable-ident-lookups \
	--enable-linux-netfilter \
	--enable-removal-policies=heap,lru \
	--enable-snmp \
	--enable-storeio=aufs,diskd,ufs,rock \
	--enable-wccpv2 \
	--enable-esi \
	--enable-ssl-crtd \
	--enable-icmp \
	--with-aio \
	--with-default-user=squid \
	--with-filedescriptors=16384 \
	--with-dl \
	--with-openssl \
	--with-pthreads \
	--with-included-ltdl \
	--disable-arch-native \
	--without-nettle \
	build_alias=x86_64-redhat-linux-gnu \
	host_alias=x86_64-redhat-linux-gnu \
	target_alias=x86_64-redhat-linux-gnu \
	CXXFLAGS='-O2 -g -pipe -Wall -Wp,-D_FORTIFY_SOURCE=2 -fexceptions -fstack-protector --param=ssp-buffer-size=4 -m64 -mtune=generic -fPIC' \
	PKG_CONFIG_PATH=:/usr/lib64/pkgconfig:/usr/share/pkgconfig \
  --enable-ltdl-convenience
echo "Executing make and install"


groupadd -g 23 squid
useradd -u 23 -g 23 -d /var/spool/squid -r -s /bin/false -c "Squid Proxy Server" squid
mkdir -p /var/spool/squid
chown squid:squid /var/spool/squid
mkdir -p /var/log/squid
chown squid:squid /var/log/squid


make && make install
echo "Activating squid service"

# Ok, this was tricky.
# Squid initially starts up with the limit defined by the systemd and ignores system ulimits (1024)
# The squid.conf specified something higher, but this doesn't take effect as squid is started before the config file lands
# So, max file descriptors is needed here in systemd service config and is no longer required in the squid.conf,
# but they are both set to 65k so no problem with it remaining.

mkdir -p /etc/systemd/system/squid.service.d
cp /tmp/files/numfiles.conf /etc/systemd/system/squid.service.d/numfiles.conf

cp ./tools/systemd/squid.service /etc/systemd/system/
systemctl enable squid

# cp /tmp/files/etc/logrotate.d/squid /etc/logrotate.d/squid
# chmod 644 /etc/logrotate.d/squid

# chkconfig squid on
