template {
  source = "/etc/consul-template/templates/modsec_update.sh.ctmpl"
  destination = "/etc/cron.daily/modsec_update.sh"
  command = "/bin/bash -c 'chmod +x /etc/cron.daily/modsec_update.sh'"
  perms = 0755
}
