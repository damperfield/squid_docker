#!/usr/bin/env bash
set -euxo pipefail

function section {
  echo "###########################################################"
  echo "###########################################################"
  echo "$1"
  echo "###########################################################"
  echo "###########################################################"
}

ls -al /tmp/files/
cp /tmp/files/epel.repo /etc/yum.repos.d/

packages=(
  libtool
  yajl-devel
  GeoIP-devel
  pcre-devel
  openssl-devel
)

yum clean all
yum groupinstall -y 'Development Tools'
yum -y install ${packages[*]}
