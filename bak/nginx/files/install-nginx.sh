#!/usr/bin/env bash
set -exuo pipefail

function section {
  echo "###########################################################"
  echo "###########################################################"
  echo "$1"
  echo "###########################################################"
  echo "###########################################################"
}

section "Configuring ModSecurity"
# bad PATH=/opt/rh/devtoolset-2/root/usr/bin:/usr/local/bin:usr/local/sbin:$PATH
PATH=/usr/local/bin:$PATH
MODSEC_VERSION=3/master
# NGINX_VERSION=1.15.8
NGINX_VERSION=1.14.2

packages=(
  libxml2-devel
  doxygen
  zlib-devel
  git
  flex
  httpd-devel
  libcurl-devel
  openssl-devel
  libyaml-devel
)
yum install -y ${packages[*]}

git clone -b v${MODSEC_VERSION} --single-branch --depth 1 https://github.com/SpiderLabs/ModSecurity /usr/src/modsec
cd /usr/src/modsec
sh build.sh
git submodule init
git submodule update

./configure --with-yajl=yes --with-geoip=yes --disable-mlogc
make -j4 && make install
cd /usr/src/ && git clone --depth 1 https://github.com/SpiderLabs/ModSecurity-nginx.git


section "Installing Nginx ${NGINX_VERSION}"

# cd /usr/src && wget https://artefactrepository.service.ops.iptho.co.uk/repository/EBSA/nginx-$NGINX_VERSION.tar.gz
cd /usr/src && wget https://nginx.org/download/nginx-${NGINX_VERSION}.tar.gz 
tar -zxf nginx-$NGINX_VERSION.tar.gz && rm -f nginx-$NGINX_VERSION.tar.gz
cd /usr/src/nginx-$NGINX_VERSION && ./configure --add-module=/usr/src/ModSecurity-nginx --with-http_ssl_module --with-stream --with-http_v2_module --with-threads --with-file-aio
make -j4  && make install
ln -s /usr/local/nginx/sbin/nginx /usr/sbin/nginx

# LEFT HERE
cp /usr/src/modsec/modsecurity.conf-recommended /usr/local/nginx/conf/modsecurity.conf
cp /usr/src/modsec/unicode.mapping /usr/local/nginx/conf/
mkdir /usr/local/nginx/conf/owasp-modsecurity-crs
git clone https://github.com/SpiderLabs/owasp-modsecurity-crs.git /usr/local/nginx/conf/owasp-modsecurity-crs/.
pushd /usr/local/nginx/conf/owasp-modsecurity-crs
git fetch
git checkout remotes/origin/v3.1/dev -- util/upgrade.py
popd
cp /tmp/files/modsec/crs-setup.conf /usr/local/nginx/conf/owasp-modsecurity-crs/.
cp /tmp/files/modsec/REQUEST-900-EXCLUSION-RULES-BEFORE-CRS.conf /usr/local/nginx/conf/owasp-modsecurity-crs/rules/.
cp /tmp/files/modsec/RESPONSE-999-EXCLUSION-RULES-AFTER-CRS.conf /usr/local/nginx/conf/owasp-modsecurity-crs/rules/.
cp /tmp/files/modsec/modsec_includes.conf /usr/local/nginx/conf/.
cp /tmp/files/modsec/modsecurity.conf /usr/local/nginx/conf/.
mkdir /var/log/modsec
cp /tmp/files/nginx/nginx.conf /usr/local/nginx/conf/.
#ln -s /usr/local/lib/libyajl.so.2 /usr/lib64/libyajl.so.2
ldconfig


rm -f /opt/*.tar.gz
rm -f /usr/src/*.tar.gz

section "Configuring NGINX"
PATH=/usr/local/bin:usr/local/sbin:$PATH
useradd -UM nginx
chown -R nginx:nginx /var/www
rm -rf /usr/local/nginx/html
# Error pages
# generic_401.html
# generic_413.html
# generic_5xx.html
mkdir -p /usr/local/nginx/www/error_pages/ && {
    mv /tmp/files/nginx/generic*.html /usr/local/nginx/www/error_pages/
}
cp /tmp/files/modsec/upgrade.py /usr/local/nginx/conf/owasp-modsecurity-crs/util/.
cp /tmp/files/modsec/modsec_housekeeping.sh /etc/cron.daily/
chmod +x /etc/cron.daily/modsec_housekeeping.sh /usr/local/nginx/conf/owasp-modsecurity-crs/util/upgrade.py
cp /tmp/files/modsec/modsec_update.sh.ctmpl /etc/consul-template/templates/modsec_update.sh.ctmpl
cp /tmp/files/modsec/modsec_update.hcl /etc/consul-template/config/modsec_update.sh.hcl

cp /tmp/files/nginx/nginx.service /etc/systemd/system/nginx.service
chmod 644 /etc/systemd/system/nginx.service
systemctl enable nginx.service

# cp /tmp/files/logrotate/nginx /etc/logrotate.d/nginx
# chmod 644 /etc/logrotate.d/nginx

chown -R nginx:root /usr/local/modsecurity
chown -R nginx:root /usr/local/nginx
chown -R nginx:root /var/log/modsec
chmod a+rx /usr/local/nginx/logs

# Mod security log file is created by the root nginx process
# which causes problems with logrotate
# Ensure the group is nginx and setgid is set
chown -R nginx:nginx /var/log/modsec
chmod g+s /var/log/modsec

echo 'net.ipv4.tcp_fin_timeout = 30' >> /etc/sysctl.conf
echo 'net.ipv4.ip_local_port_range = 1024 65000' >> /etc/sysctl.conf
echo 'fs.file-max = 65535' >> /etc/sysctl.conf

mkdir -p /usr/local/nginx/logs
chown nginx /usr/local/nginx/logs

# Try to update the rules. If there's nothing to update the command will fail, hence the || true
/usr/local/nginx/conf/owasp-modsecurity-crs/util/upgrade.py --crs || true

# setfacl -R -m user:zabbix:r-x /usr/local/nginx/
