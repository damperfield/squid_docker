#!/usr/bin/env bash

set -e

function section {
  echo "###########################################################"
  echo "###########################################################"
  echo "$1"
  echo "###########################################################"
  echo "###########################################################"
}

yum clean all
yum groupinstall -y 'Development Tools'

yum install -y wget gcc openssl-devel pyOpenSSL bzip2-devel perl-Archive-Tar perl-Crypt-SSLeay perl-libwww-perl.noarch perl autoconf automake make libxml2-devel libcap-devel
